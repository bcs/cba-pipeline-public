"""sparse estimation module for sparse linear regression.

Example
-------

Implemented Algorithms
----------------------

"""
from packages.sparseEstimation import GbRegression

__all__ = ["GbRegression.py"]
